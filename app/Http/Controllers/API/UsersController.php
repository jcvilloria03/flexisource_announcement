<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use App\UserBalanceCreditDebit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{
    use VerifiesEmails;
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        /*
        if (strtolower($user->user_type) !== 'admin') {
            abort(401,'Unauthorized');
        }
        */
        $perPage = $request->input('per_page');
        $orderBy = $request->input('sort_by');
        $includes = $request->input('includes');
        $page = $request->input('page');
        $query = User::where('email', '!=', null);

        if (!empty($orderBy)) {
            $sort = explode(".", $orderBy);
            $query->orderBy($sort[0], $sort[1]);
        }

        $data = $query->paginate(!empty($perPage) ? $perPage : $query->count(), ['*'], 'page', !empty($page) ? $page : 1);
        return $data;
    }

    public function getUser(Request $request)
    {
        $user = User::select('id', 'name', 'email')->findOrFail($request->user()->id);
        return response($user);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        //

        $login = $request->validate(
            [
                'email' => 'required|string',
                'password' => 'required|string'
            ]
        );
        //$login['user_type'] = 'bettor';
        //var_dump($login);die();
        
        if (!Auth::attempt($login)) {
            return abort(404, 'Invalid Login Credentials');
        } else {
            $user = Auth::user();
            if ($user->email_verified_at == null) {
                abort(401, 'Unable to login. Email not yet verified');
            }
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response(['user' => Auth::user()]);
    }

    public function getUserById(Request $request, $id)
    {
        $user = $request->user();
        if (strtolower($user->user_type) !== 'admin') {
            abort(401,'Unauthorized');
        }
        $data = User::select('name', 'email', 'mobile_number', 'balance')->findOrFail($id);
        return $data;
    }

    public function updateUser(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required|string|max:100',
                'email' => 'required|string|email|unique:App\User,email,' . $id . '|max:100',
            ]
        );

        $updatedByUser = $request->user();
        if (strtolower($updatedByUser->user_type) !== 'admin') {
            abort(401,'Unauthorized');
        }

        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        Log::debug("Update user: " . $user);
        $user->save();
        Log::debug("Succesfully updated user: " . $user);
        Log::debug("Updated by: " . $updatedByUser);

        return response(200);
    }

    public function getUserAccountHistory(Request $request)
    {
        $perPage = $request->input('per_page');
        $orderBy = $request->input('sort_by'); //Index
        $page = $request->input('page');
        $filter = json_decode($request->input('filter'), true);

        if (!empty($orderBy)) {
            $sort = explode(".", $orderBy);
            $query->orderBy($sort[0], $sort[1]);
        }


        return $query->paginate(!empty($perPage) ? $perPage : 10, ['*'], 'page', !empty($page) ? $page : 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signUp(Request $request)
    {
        //

        $request->validate(
            [
                'name' => 'required|string|max:100',
                'email' => 'required|string|email|unique:App\User,email|max:100',
                'password' => 'required|string',
            ]
        );

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'api_token' => uniqid(),
        ]);

        // $user->sendApiEmailVerificationNotification();

        $success['message'] = 'Please confirm by clicking on verify button sent to your email';

        return response()->json(['success' => $success], $this->successStatus);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
