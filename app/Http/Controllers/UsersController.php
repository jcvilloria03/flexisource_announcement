<?php

namespace App\Http\Controllers;

use App\Bet;
use App\User;
use App\UserBalanceCreditDebit;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        return view('users');
    }

    public function viewUser($id)
    {
        return view('user-view', ['id' => $id]);
    }

    public function editUser($id)
    {
        return view('user-edit', ['id' => $id]);
    }

    public function getUserBets($id)
    {
        $bets = Bet::with('user')->with('game')->with('team_subtype')->where('user_id', $id)->get();
        if ($bets->isNotEmpty()) {
            return $bets;
        } else {
            abort(404, 'No bets found');
        }
    }

    public function index(Request $request)
    {
        $perPage = $request->input('per_page');
        $orderBy = $request->input('sort_by');
        $includes = $request->input('includes');
        $page = $request->input('page');
        $query = User::where('email', '!=', null);

        if (!empty($orderBy)) {
            $sort = explode(".", $orderBy);
            $query->orderBy($sort[0], $sort[1]);
        }

        $data = $query->paginate(!empty($perPage) ? $perPage : $query->count(), ['*'], 'page', !empty($page) ? $page : 1);
        return $data;
    }

    public function get($userId)
    {
        $user = User::select('id', 'name', 'email')->where('id', $userId)->first();
        if (!empty($user)) {
            return $user;
        } else {
            abort(404, 'User not found');
        }
        return $user;
    }
}
